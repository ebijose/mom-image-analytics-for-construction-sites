#!/usr/bin/python3
import gspread
from oauth2client.service_account import ServiceAccountCredentials

import urllib, re, os, logging
from time import localtime, strftime

def upload_data_to_google_drive(logger, dateTime, ipAdd, cpuTemp):
    logger.debug('starting to upload data to google drive')

    scope = ['https://spreadsheets.google.com/feeds',
             'https://www.googleapis.com/auth/drive']

    credentials = ServiceAccountCredentials.from_json_keyfile_name('/home/pi/upload_external_ip/RouterIPAddress-df5c3f269a7d.json', scope)
    gc = gspread.authorize(credentials)
    wks = gc.open("External_IP_Address_RPi")
    worksheet = wks.get_worksheet(1)
    worksheet.append_row([dateTime, ipAdd, cpuTemp])

def find_ip_address(logger):
    logger.debug('reading ip address:')
    site = urllib.request.urlopen("http://checkip.dyndns.org/").read().decode('utf-8')
    grab = re.findall('([0-9]+\.[0-9]+\.[0-9]+\.[0-9]+)', site)
    ip_address = grab[0]
    return ip_address

def getCpuTemp(logger):
    logger.debug('reading cpu temperature')
    res = os.popen('vcgencmd measure_temp').readline()
    return(res.replace("temp=","").replace("'C\n",""))

def initialise_log():
    logger = logging.getLogger('debug_log')
    logger.setLevel(logging.DEBUG)
    # create a file handler
    handler = logging.FileHandler('/home/pi/upload_external_ip/log.log')
    handler.setLevel(logging.DEBUG)

    # create a logging format
    formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
    handler.setFormatter(formatter)
    # add the handlers to the logger
    logger.addHandler(handler)
    return logger

def main():
    logger = initialise_log()
    logger.debug('=========== Starting program ===========')
    currentDateTime = strftime("%Y/%m/%d %H:%M:%S", localtime())
    logger.debug('current date time: %s', currentDateTime)
    ipAdd = find_ip_address(logger)
    logger.debug('ip address: %s', ipAdd)
    cpuTemp = getCpuTemp(logger)
    logger.debug('cpu temp: %s', cpuTemp)
    upload_data_to_google_drive(logger, currentDateTime, ipAdd, cpuTemp)
    logger.debug("=========== Uploaded data successfully ===========")

if __name__ == "__main__":
    main()