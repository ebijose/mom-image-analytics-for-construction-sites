# import the necessary packages
from picamera.array import PiRGBArray
from picamera import PiCamera
import time
import cv2
import numpy as np

import socket, struct, os
from pathlib import Path
import sys, errno

MIN = np.array([60, 50, 0],np.uint8)
MAX = np.array([140, 255, 255],np.uint8)

def upload_image(image):
    s = socket.socket()
    s.connect(('159.65.134.155', 5001))
    file_path = os.path.abspath(image)
    print(file_path)
    initial = image[0]

    # Handshake : first send file size
    size = os.path.getsize(file_path)
    print(size)
    raw_size = struct.pack("!I", size)
    tt = s.send(raw_size)
    print("response from server:", tt)
    # Send initial to distinguish between raw and processed image
    raw_initial = struct.pack("!c", initial.encode('ascii'))
    print("raw_initial", raw_initial)
    s.send(raw_initial)

    try:
        # Send file
        with open(file_path, 'rb') as f:
            print("uploading file")
            l = f.read(4096)
            while(l):
                s.send(l)
                l = f.read(4096)
            f.close()
        s.close()
        print("==== image uploaded ====")
    except IOError as e:
        if e.errno == errno.EPIPE:
            print("error:", e)
        # Handle error
def initialise_camera():
    # Look for section 3.5 for camera settings in https://picamera.readthedocs.io/en/release-1.13/recipes1.html
    # initialize the camera and grab a reference to the raw camera capture
    camera = PiCamera()
    camera.resolution = (640, 480)
    camera.rotation = 180

    # Set ISO to the desired value
    camera.iso = 50

    # Wait for the automatic gain control to settle
    time.sleep(2)
    # Now fix the values
    camera.shutter_speed = camera.exposure_speed
    camera.exposure_mode = 'off'
    g = camera.awb_gains
    camera.awb_mode = 'off'
    camera.awb_gains = g

#    camera.exposure_compensation = -2
#    camera.brightness = 40
    return camera

def capture_image(camera):
    rawCapture = PiRGBArray(camera, size=(640, 480))
    # grab an image from the camera
    camera.capture(rawCapture, format="bgr")
    image = rawCapture.array
    cv2.imwrite('RawImg.jpg', image)
    return image

def process_image(rawImg):
    blurredImg = cv2.GaussianBlur(rawImg, (7, 7), 0)
    hsv_img = cv2.cvtColor(blurredImg, cv2.COLOR_BGR2HSV)
    # find the colors within the specified boundaries and apply the mask
    mask = cv2.inRange(hsv_img, MIN, MAX)
    mask = cv2.erode(mask, None, iterations=2)
    mask = cv2.dilate(mask, None, iterations=2)
    proImg = cv2.bitwise_and(rawImg, rawImg, mask = mask)
    cv2.imwrite('ProImg.jpg', proImg)
    return proImg

def main():
    camera = initialise_camera()
    # allow the camera to warmup
    time.sleep(0.5)
    rawImg = capture_image(camera)
    proImg = process_image(rawImg)
#    file_path = os.path.abspath('RawImg.jpg')
    upload_image('RawImg.jpg')
#    file_path = os.path.abspath('ProImg.jpg')
    upload_image('ProImg.jpg')
    # show the frame
#    cv2.imshow("Frame", np.hstack([rawImg, proImg]))
#    cv2.waitKey(0)
#    cv2.destroyAllWindows()

if __name__ == "__main__":
    main()