import numpy as np
import cv2
import time
import imutils
import sys
import logging
import socket, os, struct
from datetime import date
from imutils import contours
from picamera import PiCamera
from picamera.array import PiRGBArray

MIN = np.array([60, 50, 0], np.uint8)
MAX = np.array([140, 255, 255], np.uint8)

kernel = cv2.getStructuringElement(cv2.MORPH_ELLIPSE,(5,5))
fgbg = cv2.createBackgroundSubtractorMOG2(history = 10, varThreshold = 64, detectShadows = False)

MAX_FEATURES = 500
GOOD_MATCH_PERCENT = 0.15


def flag_file():
    fileName = time.strftime("%Y%m%d")
    fileName = fileName + '.txt'

    try:
        fh = open(fileName, 'r+')
        flagImg = 'Ya'
        fh.write(flagImg)
        # Store configuration file values
    except FileNotFoundError:
    # Keep preset values
        fh = open(fileName, "w+")
        flagImg = 'No'
        fh.write(flagImg)

    return flagImg


def upload_image(image):
    s = socket.socket()
    s.connect(('159.65.134.155', 5001))
    file_path = os.path.abspath(image)

    fileName = os.path.basename(file_path)
    initial = fileName[0]

    # Handshake : first send file size
    size = os.path.getsize(file_path)

    raw_size = struct.pack("!I", size)
    tt = s.send(raw_size)
#    print("response from server:", tt)
    # Send initial to distinguish between raw and processed image
    raw_initial = struct.pack("!c", initial.encode('ascii'))
#    print("raw_initial", raw_initial)
    s.send(raw_initial)

    try:
        # Send file
        with open(file_path, 'rb') as f:
            logger.info('Uploading image....')
            l = f.read(4096)
            while(l):
                s.send(l)
                l = f.read(4096)
            f.close()
        s.close()
        logger.info('---- Image uploaded ----')
    except IOError as e:
        if e.errno == errno.EPIPE:
            logger.info('error : %s', e)


def alignImages(im1, im2):
    # Convert images to grayscale
    im1Gray = cv2.cvtColor(im1, cv2.COLOR_BGR2GRAY)
    im2Gray = cv2.cvtColor(im2, cv2.COLOR_BGR2GRAY)

    # Detect ORB features and compute descriptors.
    orb = cv2.ORB_create(MAX_FEATURES)
    keypoints1, descriptors1 = orb.detectAndCompute(im1Gray, None)
    keypoints2, descriptors2 = orb.detectAndCompute(im2Gray, None)

    # Match features.
    matcher = cv2.DescriptorMatcher_create(cv2.DESCRIPTOR_MATCHER_BRUTEFORCE_HAMMING)
    matches = matcher.match(descriptors1, descriptors2, None)

    # Sort matches by score
    matches.sort(key=lambda x: x.distance, reverse=False)

    # Remove not so good matches
    numGoodMatches = int(len(matches) * GOOD_MATCH_PERCENT)
    matches = matches[:numGoodMatches]

    # Draw top matches
    imMatches = cv2.drawMatches(im1, keypoints1, im2, keypoints2, matches, None)
#    cv2.imwrite("matches.jpg", imMatches)

    # Extract location of good matches
    points1 = np.zeros((len(matches), 2), dtype=np.float32)
    points2 = np.zeros((len(matches), 2), dtype=np.float32)

    for i, match in enumerate(matches):
        points1[i, :] = keypoints1[match.queryIdx].pt
        points2[i, :] = keypoints2[match.trainIdx].pt

    # Find homography
    h, mask = cv2.findHomography(points1, points2, cv2.RANSAC)

    # Use homography
    height, width, channels = im2.shape
    im1Reg = cv2.warpPerspective(im1, h, (width, height))

    return im1Reg, h

def process_image(rawImg):
#    blurredImg = cv2.GaussianBlur(rawImg, (5, 5), 0)
    hsv_img = cv2.cvtColor(rawImg, cv2.COLOR_BGR2HSV)

    # find the colors within the specified boundaries and apply the mask
    mask = cv2.inRange(hsv_img, MIN, MAX)
    mask = cv2.erode(mask, None, iterations=2)
    mask = cv2.dilate(mask, None, iterations=2)
    proImg = cv2.bitwise_and(rawImg, rawImg, mask = mask)
    return proImg


def background_subtraction(img):
#    proImg = process_image(img)
    proImg = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

    fgmask = fgbg.apply(proImg) #, fgmask, 0.005)
    fgmask = cv2.morphologyEx(fgmask, cv2.MORPH_OPEN, kernel)
    # the countour marking is taken from
    # https://www.pyimagesearch.com/2016/10/31/detecting-multiple-bright-spots-in-an-image-with-python-and-opencv/
    # find the contours in the mask, then sort them from left to right
    cnts = cv2.findContours(fgmask.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
    cnts = cnts[0] if imutils.is_cv2() else cnts[1]
    if(cnts):
        cnts = contours.sort_contours(cnts)[0]

    # loop over the contours
    for (i, c) in enumerate(cnts):
        # draw the bright spot on the image
        (x, y, w, h) = cv2.boundingRect(c)
        ((cX, cY), radius) = cv2.minEnclosingCircle(c)
        cv2.circle(img, (int(cX), int(cY)), int(radius), (0, 0, 255), 3)

    return img, fgmask


def initialise_camera():
    # Look for section 3.5 for camera settings in https://picamera.readthedocs.io/en/release-1.13/recipes1.html
    # initialize the camera and grab a reference to the raw camera capture
    camera = PiCamera()
    camera.resolution = (640, 480)
    camera.rotation = 180

    # Set ISO to the desired value
    camera.iso = 50

    # Wait for the automatic gain control to settle
    time.sleep(2)
    # Now fix the values
    camera.shutter_speed = camera.exposure_speed
    camera.exposure_mode = 'off'
    g = camera.awb_gains
    camera.awb_mode = 'off'
    camera.awb_gains = g

    return camera

def capture_image(camera, imgName):
    rawCapture = PiRGBArray(camera, size=(640, 480))
    # grab an image from the camera
    camera.capture(rawCapture, format="bgr")
    image = rawCapture.array
    cv2.imwrite(imgName, image)
    return image


def main():
    # obtain whether the unit is taking the first photo of the day. This is to keep the first photo 
    # as reference and all the other photos taken during that day will be compensated for motion based
    # on the reference photo.
    flagImg = flag_file()
    logger.info('Image flag status %s', flagImg)

    # initialise camera and also to compensate for exposure
    camera = initialise_camera()
    logger.info('Initialised camera')

    # allow the camera to warmup
    time.sleep(0.5)
    # Load reference image. The first picture taken when the unit is powered on will be treated as reference image
    if flagImg == 'No':
        refImg = capture_image(camera, 'refImg.jpg')
        logger.info('Captured ref image')

        if cv2.mean(refImg) >= (75, 75, 75, 0):
            flagImg = 'Ya'
            logger.info('Ref image captured')
        else:
            logger.info('Ref image is underexposed, will capture again.')
    else:
        # Load an image
        rawImg = capture_image(camera, 'rawImg.jpg')
        logger.info('Captured raw image')

        if cv2.mean(rawImg) >= (75, 75, 75, 0):
#            img, h = alignImages('rawImg.jpg', 'refImg.jpg')
#            img, h = alignImages(refImg, rawImg)
#            proImg = process_image(img)
            proImg = process_image(rawImg)
            logger.info('safety barriers are extracted')
            img, fgmask = background_subtraction(proImg)
            logger.info('completed background subtraction')

            cv2.imwrite('R_frame.jpg', img)
            cv2.imwrite('P_frame.jpg', proImg)
#            logger.info('1) image with detection marked and 2) background subtraction mask are saved')

#            imgPathRaw = '/home/pi/bg_sub/' + 'R_frame.jpg'
#            imgPathPro = '/home/pi/bg_sub/' + 'P_frame.jpg'

##            upload_image('R_frame.jpg')
##            upload_image('P_frame.jpg')
#            logger.info('uploaded image with detection marked')
#            upload_image(imgPathPro)
#            logger.info('uploaded image with safety barriers')
        else:
            logger.info('raw image is underexposed, will capture again')


if __name__ == "__main__":

    logger = logging.getLogger('/home/pi/bg_sub/logs/'+ str(date.today()))
    hdlr = logging.FileHandler('/home/pi/bg_sub/logs/'+ str(date.today()) + '.log')
    formatter = logging.Formatter('%(asctime)s %(levelname)s %(message)s')

    hdlr.setFormatter(formatter)
    logger.addHandler(hdlr)
    logger.setLevel(logging.DEBUG)
    logger.info('======= Starting the program =======')

    main()
    logger.info('======= Finished executing script =======')
